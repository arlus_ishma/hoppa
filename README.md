`HOPPA`
=====

Hoppa is a platform for monitoring fish pond conditions based on certain metrics. 



##`STACK`

Bundled with the system is an ansible playbook for easy deployment.
It can install and configure these applications that are commonly used in production Django deployments:

- Nginx
- Gunicorn
- PostgreSQL
- Supervisor
- Virtualenv
- Memcached
- Celery
- RabbitMQ

Default settings are stored in ```roles/role_name/vars/main.yml```. 
Environment-specific settings are in the ```env_vars``` directory.

A `certbot` role is also included for automatically generating and renewing trusted SSL certificates with [Let's Encrypt](https://letsencrypt.org/). 

**Tested with OS:** Ubuntu 16.04 LTS x64

**Tested with Cloud Providers:** [Digital Ocean](https://www.digitalocean.com/?refcode=5aa134a379d7), [Amazon](https://aws.amazon.com), [Rackspace](http://www.rackspace.com/)

### Requirements

- [Ansible](http://docs.ansible.com/intro_installation.html)


