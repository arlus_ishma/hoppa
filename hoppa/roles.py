from rolepermissions.roles import AbstractUserRole


class GeneralAdmin(AbstractUserRole):
    available_permissions = {
        'manage_county_admin': True,
        'manage_farmer': True,
        'manage_questionnaire': True,
        'general_dashboard': True
    }


class CountyAdmin(AbstractUserRole):
    available_permissions = {
        'manage_farmer': True,
        'county_dashboard': True
    }


class Farmer(AbstractUserRole):
    available_permissions = {
        'manage_questionnaire': True,
        'farmer_dashboard': True
    }
