from django.contrib.auth import authenticate, login
from django.http.response import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.utils import timezone
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.checkers import has_permission
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet
from django.contrib.auth.models import User
from hoppa.apps.profiles.models import UserProfile
from hoppa.apps.profiles.forms import UserProfileInline as FarmerProfileInline, CustomUserChangeForm, GeneralUserProfileInline
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from hoppa.apps.questionnaires.models import Questionnaire


class UserListView(HasRoleMixin, ListView):

    model = User
    template_name = "profiles/list.html"
    allowed_roles = "general_admin"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context


class FarmerListView(HasRoleMixin, ListView):

    model = User
    template_name = "farmers/list.html"
    allowed_roles = "general_admin"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(FarmerListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context

    def get_queryset(self):
        queryset = User.objects.filter(user_account__role='Farmer')
        return queryset


class CountyFarmerListView(HasRoleMixin, ListView):

    model = User
    template_name = "farmers/list.html"
    allowed_roles = "county_admin"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(CountyFarmerListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context

    def get_queryset(self):
        queryset = User.objects.filter(user_account__role="Farmer", user_account__admin=self.request.user)
        return queryset


class CountyRepsListView(HasRoleMixin, ListView):

    model = User
    template_name = "county_reps/list.html"
    allowed_roles = "general_admin"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(CountyRepsListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context

    def get_queryset(self):
        queryset = User.objects.filter(user_account__role='County Admin')
        return queryset


def user_login(request):
    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # We'll send the user back to his relevant dashboard based on permissions.
                login(request, user)
                if has_permission(user, 'general_dashboard'):
                    return HttpResponseRedirect(reverse('general-dashboard'))
                elif has_permission(user, 'county_dashboard'):
                    return HttpResponseRedirect(reverse('county-dashboard'))
                else:
                    return HttpResponseRedirect(reverse('farmer-dashboard'))
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
            return HttpResponse("Invalid login details supplied.")
    else:
        return render(request, 'profiles/login.html')

#
# class CreateUserView(HasRoleMixin, CreateWithInlinesView):
#     model = User
#     inlines = [UserProfileInline]
#     form_class = UserCreationForm
#     template_name = "profiles/create.html"
#     allowed_roles = "general_admin"
#
#     def get_context_data(self, **kwargs):
#         context = super(CreateUserView, self).get_context_data(**kwargs)
#         context['now'] = timezone.now()
#         context['profile'] = self.request.user
#         return context
#
#     def get_success_url(self):
#         return reverse('update-user', args=(self.object.id,))


class CreateUserView(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "profiles/create.html"
    http_method_names = ["POST", "GET"]

    def get_context_data(self, **kwargs):
        context = super(CreateUserView, self).get_context_data(**kwargs)
        context['inline'] = GeneralUserProfileInline
        context['profile'] = self.request.user
        return context

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_county_admin'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = GeneralUserProfileInline(request.POST, request.FILES)
        form_valid = form.is_valid()
        formset_valid = formset.is_valid()

        if form_valid and formset_valid:
            self.object = form.save()
            profile = formset.save(commit=False)
            profile.user = self.object
            profile.admin = request.user
            profile.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('update-user', args=(self.object.id,))


# class UpdateUserView(HasRoleMixin, UpdateWithInlinesView):
#     model = User
#     inlines = [UserProfileInline]
#     form_class = UserChangeForm
#     template_name = "profiles/update.html"
#     allowed_roles = "general_admin"
#
#     def get_context_data(self, **kwargs):
#         context = super(UpdateUserView, self).get_context_data(**kwargs)
#         context['now'] = timezone.now()
#         context['profile'] = self.request.user
#         return context
#
#     def get_success_url(self):
#         return reverse('update-user', args=(self.object.id,))


class UpdateUserView(UpdateView):
    model = User
    form_class = CustomUserChangeForm
    template_name = "profiles/update.html"
    http_method_names = ["POST", "GET"]

    def get_context_data(self, **kwargs):
        context = super(UpdateUserView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        if self.request.POST:
            context['inline'] = GeneralUserProfileInline(self.request.POST, instance=self.object.user_account)
        else:
            context['inline'] = GeneralUserProfileInline(instance=self.object.user_account)
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        inline = GeneralUserProfileInline(instance=self.object.user_account)
        return self.render_to_response(self.get_context_data(form=form, inline=inline))

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_county_admin'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        request.POST = request.POST.copy()
        request.POST['admin'] = request.user

        return super(UpdateUserView, self).post(request, **kwargs)

    def get_success_url(self):
        return reverse('update-user', args=(self.object.id,))


class DeleteUserView(HasRoleMixin, DeleteView):
    model = User
    allowed_roles = "general_admin"
    template_name = "profiles/delete.html"

    def get_success_url(self):
        return reverse('list-users', args=(self.object.id,))


class FarmerCreateView(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "farmers/create.html"
    http_method_names = ["POST", "GET"]

    def get_context_data(self, **kwargs):
        context = super(FarmerCreateView, self).get_context_data(**kwargs)
        context['inline'] = FarmerProfileInline
        context['profile'] = self.request.user
        return context

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_farmer'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        copy = self.request.POST.copy()
        copy.update({'role': 'Farmer'})
        formset = FarmerProfileInline(copy, request.FILES)
        form_valid = form.is_valid()
        formset_valid = formset.is_valid()

        if form_valid and formset_valid:
            self.object = form.save()
            profile = formset.save(commit=False)
            profile.user = self.object
            profile.role = 'Farmer'
            profile.admin = request.user
            profile.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('update-farmer', args=(self.object.id,))


class FarmerUpdateView(UpdateView):
    model = User
    form_class = CustomUserChangeForm
    template_name = "farmers/update.html"
    http_method_names = ["POST", "GET"]

    def get_context_data(self, **kwargs):
        context = super(FarmerUpdateView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        if self.request.POST:
            context['inline'] = FarmerProfileInline(self.request.POST, instance=self.object.user_account)
        else:
            context['inline'] = FarmerProfileInline(instance=self.object.user_account)
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        inline = FarmerProfileInline(instance=self.object.user_account)
        return self.render_to_response(self.get_context_data(form=form, inline=inline))

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_farmer'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        request.POST = request.POST.copy()
        request.POST['role'] = 'Farmer'
        request.POST = request.POST.copy()
        request.POST['role'] = 'Farmer'
        request.POST['user'] = self.object
        request.POST['admin'] = request.user
        return super(FarmerUpdateView, self).post(request, **kwargs)

    def get_success_url(self):
        return reverse('update-farmer', args=(self.object.id,))


class FarmerDetailView(DetailView):
    model = User
    template_name = "farmers/detail.html"

    def get_context_data(self, **kwargs):
        context = super(FarmerDetailView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        try:
            context['questionnaire'] = Questionnaire.objects.get(farmer=self.request.user)
        except TypeError:
            pass
        except Questionnaire.DoesNotExist:
            pass
        return context


class UserDetailView(DetailView):
    model = User
    template_name = "county_reps/detail.html"

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context


class FarmerDeleteView(DeleteView):
    model = User
    template_name = "farmers/delete.html"

    def get_success_url(self):
        return reverse('list-farmers', args=(self.object.id,))
