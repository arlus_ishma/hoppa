from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from rolepermissions.roles import assign_role


ROLE_CHOICES = (
    ('General Admin', 'General Admin'),
    ('County Admin', 'County Admin'),
    ('Farmer', 'Farmer')
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_account")
    profile_picture = models.FileField(upload_to='profile_photos/%Y/%m/%d/')
    location = models.CharField(max_length=200)
    role = models.CharField(choices=ROLE_CHOICES, max_length=30)
    admin = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="user_admin")

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('update-user', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        super(UserProfile, self).save(*args, **kwargs)
        if self.role == 'General Admin':
            assign_role(self.user, 'general_admin')
        elif self.role == 'County Admin':
            assign_role(self.user, 'county_admin')
        else:
            assign_role(self.user, 'farmer')

