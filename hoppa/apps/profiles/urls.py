from django.conf.urls import url
from .views import UserListView, user_login, CreateUserView, UpdateUserView, DeleteUserView, FarmerCreateView, \
    FarmerUpdateView, FarmerDeleteView, FarmerListView, CountyRepsListView, FarmerDetailView, UserDetailView, CountyFarmerListView
from django.contrib.auth.views import logout

urlpatterns = [
    url(r'^$', UserListView.as_view(), name="list-users"),
    url(r'^login/$', user_login, name="login"),
    url(r'^logout/$', logout, {'next_page': '/profile/login'}, name='logout'),
    url(r'^user/new/$', CreateUserView.as_view(), name="create-user"),
    url(r'^user/(?P<pk>\d+)/$', UpdateUserView.as_view(), name="update-user"),
    url(r'^user/(?P<pk>\d+)/details/$', UserDetailView.as_view(), name="user-details"),
    url(r'^user/delete/(?P<pk>\d+)/$', DeleteUserView.as_view(), name="delete-user"),
    url(r'^farmers/$', FarmerListView.as_view(), name="list-farmers"),
    url(r'^farmers/county/$', CountyFarmerListView.as_view(), name="list-county-farmers"),
    url(r'^county-reps/$', CountyRepsListView.as_view(), name="list-county-reps"),
    url(r'^farmer/new/$', FarmerCreateView.as_view(), name="create-farmer"),
    url(r'^farmer/(?P<pk>\d+)/$', FarmerUpdateView.as_view(), name="update-farmer"),
    url(r'^farmer/(?P<pk>\d+)/details/$', FarmerDetailView.as_view(), name="farmer-details"),
    url(r'^farmer/delete/(?P<pk>\d+)/$', FarmerDeleteView.as_view(), name="delete-farmer"),
]
