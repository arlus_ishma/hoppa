from django.contrib.auth.forms import UserChangeForm
from django.forms import ModelForm
from hoppa.apps.profiles.models import User, UserProfile
from django import forms

choices = (
    ("County Admin", "County Admin"),
    ("General Admin", "General Admin"),
    ("Farmer", "Farmer")
)


class UserProfileInline(ModelForm):
    role = forms.ChoiceField(choices=choices, widget=forms.Select(attrs={'class': 'form-control'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = UserProfile
        exclude = ["role", "user", "admin"]


class GeneralUserProfileInline(ModelForm):
    role = forms.ChoiceField(choices=choices, widget=forms.Select(attrs={'class': 'form-control'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = UserProfile
        exclude = ["user", "admin", "password"]


class UserUpdateForm(ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class CustomUserChangeForm(UserChangeForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))


    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')

    def clean_password(self):
        return "" # This is a temporary fix for a django 1.4 bug
