from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.utils import timezone
from hoppa.apps.ponds.models import Pond
from rolepermissions.mixins import HasRoleMixin


class PondListView(HasRoleMixin, ListView):

    model = Pond
    template_name = "ponds/list.html"
    allowed_roles = "general_admin"

    def get_context_data(self, **kwargs):
        context = super(PondListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class PondCreateView(CreateView):
    model = Pond
    fields = ['owner']


class PondUpdateView(UpdateView):
    model = Pond
    fields = ['owner']


class PondDeleteView(DeleteView):
    model = Pond
    success_url = reverse_lazy('list-ponds')
