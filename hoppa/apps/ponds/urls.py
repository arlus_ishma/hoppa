from django.conf.urls import url
from .views import PondListView


urlpatterns = [
    url(r'^$', PondListView.as_view(), name="list-ponds"),
    # url(r'pond/add/$', PondCreateView.as_view(), name='pond-add'),
    # url(r'pond/(?P<pk>[0-9]+)/$', PondUpdateView.as_view(), name='pond-update'),
    # url(r'pond/(?P<pk>[0-9]+)/delete/$', PondDeleteView.as_view(), name='pond-delete'),
]
