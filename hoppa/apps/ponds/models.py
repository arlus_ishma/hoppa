from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


class Pond(models.Model):
    owner = models.ForeignKey(User)

    def __str__(self):
        return self.owner.username

    def get_absolute_url(self):
        return reverse('pond-detail', kwargs={'pk': self.pk})
