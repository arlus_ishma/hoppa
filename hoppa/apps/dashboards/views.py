from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView
from rolepermissions.checkers import has_permission
from rolepermissions.mixins import HasRoleMixin


class GeneralDashboardView(HasRoleMixin, TemplateView):

    template_name = "dashboards/general.html"
    allowed_roles = "general_admin"

    def get_context_data(self, **kwargs):
        context = super(GeneralDashboardView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context


class CountyDashboardView(HasRoleMixin, TemplateView):

    template_name = "dashboards/county.html"
    allowed_roles = "county_admin"

    def get_context_data(self, **kwargs):
        context = super(CountyDashboardView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context


class FarmerDashboardView(HasRoleMixin, TemplateView):

    template_name = "dashboards/farmer.html"
    allowed_roles = "farmer"

    def get_context_data(self, **kwargs):
        context = super(FarmerDashboardView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context


def dashboard_selector(request):
    if request.user.is_authenticated():
        # Is the account active? It could have been disabled.
        if request.user.is_active:
            if has_permission(request.user, 'general_dashboard'):
                return HttpResponseRedirect(reverse('general-dashboard'))
            elif has_permission(request.user, 'county_dashboard'):
                return HttpResponseRedirect(reverse('county-dashboard'))
            else:
                return HttpResponseRedirect(reverse('farmer-dashboard'))
        else:
            # An inactive account was used - no logging in!
            return HttpResponse("Your account is disabled.")
    else:
        return render(request, 'profiles/login.html')
