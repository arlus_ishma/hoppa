from django.conf.urls import url
from .views import GeneralDashboardView, CountyDashboardView, FarmerDashboardView, dashboard_selector


urlpatterns = [
    url(r'^$', dashboard_selector, name="dashboard-selector"),
    url(r'^general/$', GeneralDashboardView.as_view(), name="general-dashboard"),
    url(r'^county/$', CountyDashboardView.as_view(), name="county-dashboard"),
    url(r'^farmer/$', FarmerDashboardView.as_view(), name="farmer-dashboard")
]
