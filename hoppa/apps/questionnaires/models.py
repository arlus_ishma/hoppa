from django.db import models
from django.contrib.auth.models import User

CHOICES = (
    ('Choice 1', 'Choice1'),
    ('Choice 2', 'Choice2'),
    ('Choice 3', 'Choice3')
)


class Questionnaire(models.Model):
    farmer = models.OneToOneField(User)
    question1 = models.CharField(max_length=50, blank=True)
    question2 = models.CharField(max_length=50, choices=CHOICES, blank=True)
    question3 = models.NullBooleanField(blank=True, null=True)
    submitted = models.BooleanField(default=False)

    def __str__(self):
        return self.farmer.username
