from django.conf.urls import url
from .views import QuestionnaireListView, QuestionnaireCreateView, QuestionnaireUpdateView, CountyQuestionnaireListView


urlpatterns = [
    url(r'^$', QuestionnaireListView.as_view(), name="list-questionnaires"),
    url(r'^county/$', CountyQuestionnaireListView.as_view(), name="list-county-questionnaires"),
    url(r'^new/$', QuestionnaireCreateView.as_view(), name="create-questionnaire"),
    url(r'^edit/(?P<pk>\d+)/$', QuestionnaireUpdateView.as_view(), name="update-questionnaire"),
]
