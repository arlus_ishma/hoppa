from django.http.response import HttpResponseForbidden
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.utils import timezone
from rolepermissions.checkers import has_permission

from hoppa.apps.questionnaires.models import Questionnaire
from rolepermissions.mixins import HasRoleMixin
from hoppa.apps.questionnaires.forms import QuestionnaireForm


class QuestionnaireListView(HasRoleMixin, ListView):

    model = Questionnaire
    template_name = "questionnaires/list.html"
    allowed_roles = "general_admin"

    def get_context_data(self, **kwargs):
        context = super(QuestionnaireListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context


class CountyQuestionnaireListView(HasRoleMixin, ListView):

    model = Questionnaire
    template_name = "questionnaires/list.html"
    allowed_roles = "county_admin"

    def get_queryset(self):
        queryset = Questionnaire.objects.filter(farmer__user_admin=self.request.user.user_account)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(CountyQuestionnaireListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['profile'] = self.request.user
        return context


class QuestionnaireCreateView(CreateView):
    model = Questionnaire
    form_class = QuestionnaireForm
    template_name = "questionnaires/create.html"
    http_method_names = ["POST", "GET"]

    def get_context_data(self, **kwargs):
        context = super(QuestionnaireCreateView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_questionnaire'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form_valid = form.is_valid()
        if form_valid:
            if 'submit' in request.POST:
                self.object = form.save(commit=False)
                any_blank_fields = all((field.blank for field in self.object._meta.fields))
                if any_blank_fields:
                    return self.form_invalid(form)
                else:
                    self.object.submitted = True
                self.object.farmer = request.user
                self.object.save()
            self.object = form.save(commit=False)
            self.object.farmer = request.user
            self.object.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('update-questionnaire', args=(self.object.id,))


class QuestionnaireUpdateView(UpdateView):
    model = Questionnaire
    form_class = QuestionnaireForm
    template_name = "questionnaires/update.html"
    http_method_names = ["POST", "GET"]

    def get_object(self, queryset=None):
        obj = Questionnaire.objects.get(pk=self.kwargs['pk'])
        return obj

    def get_context_data(self, **kwargs):
        context = super(QuestionnaireUpdateView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        context['object'] = self.get_object()
        if self.request.POST:
            context['form'] = QuestionnaireForm(self.request.POST, instance=self.object)
        else:
            context['form'] = QuestionnaireForm(instance=self.object)
        return context

    def dispatch(self, request, *args, **kwargs):
        handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        if has_permission(self.request.user, 'manage_questionnaire') or has_permission(self.request.user, 'manage_farmer'):
            pass
        else:
            return HttpResponseForbidden()
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form_valid = form.is_valid()
        if form_valid:
            self.object = form.save(commit=False)
            any_blank_fields = all((field.blank for field in self.object._meta.fields))
            if any_blank_fields:
                pass
            else:
                self.object.submitted = True
            self.object.farmer = request.user
            self.object.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('update-questionnaire', args=(self.object.id,))
