from django.forms import ModelForm
from django import forms
from hoppa.apps.questionnaires.models import Questionnaire

q2choices = (
    ('Choice 1', 'Choice1'),
    ('Choice 2', 'Choice2'),
    ('Choice 3', 'Choice3')
)

q3choices = (
    ("True", True),
    ("False", False),
)


class QuestionnaireForm(ModelForm):
    question1 = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '5'}))
    question2 = forms.ChoiceField(choices=q2choices, widget=forms.Select(attrs={'class': 'form-control'}))
    question3 = forms.ChoiceField(choices=q3choices, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Questionnaire
        fields = ['question1', 'question2', 'question3']

