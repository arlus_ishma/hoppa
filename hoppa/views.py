from django.views.generic.base import TemplateView


class HomepageView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(HomepageView, self).get_context_data(**kwargs)
        context['profile'] = self.request.user
        return context
